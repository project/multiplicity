api = 2
core = 7.14

; Contrib modules

projects[feeds][subdir] = contrib
projects[feeds][version] = "2.0-alpha4"

projects[job_scheduler][subdir] = contrib
projects[job_scheduler][version] = "2.0-alpha3"

projects[ctools][subdir] = contrib
projects[ctools][version] = "1.0"

projects[mailhandler][subdir] = contrib
projects[mailhandler][version] = "2.5"

projects[views][subdir] = contrib
projects[views][version] = "3.3"

projects[features][subdir] = contrib
projects[features][version] = "1.0-rc2"

projects[references][subdir] = contrib
projects[references][version] = "2.0"

projects[strongarm][subdir] = contrib
projects[strongarm][version] = "2.0-rc1"

projects[cnr][subdir] = contrib
projects[cnr][version] = "4.22"

projects[diff][subdir] = contrib
projects[diff][version] = "2.0"

projects[date][subdir] = contrib
projects[date][version] = "2.5"

projects[entity][subdir] = contrib
projects[entity][version] = "1.0-rc3"

projects[title][subdir] = contrib
projects[title][version] = "1.0-alpha2"

projects[libraries][subdir] = contrib
projects[libraries][version] = "1.0"

projects[editablefields][subdir] = contrib
projects[editablefields][version] = "1.0-alpha2"
projects[editablefields][patch][] = "http://drupal.org/files/editablefields-1206656-65.patch"
projects[editablefields][patch][] = "http://drupal.org/files/editablefields-1405854-12.patch"

projects[entityreference][subdir] = contrib
projects[entityreference][version] = "1.0-rc1"

projects[eva][subdir] = contrib
projects[eva][version] = "1.1"

projects[token][subdir] = contrib
projects[token][version] = "1.1"

projects[field_permissions][subdir] = contrib
projects[field_permissions][version] = "1.0-beta2"

projects[message][subdir] = contrib
projects[message][version] = "1.3"

projects[revisionreference][subdir] = contrib
projects[revisionreference][version] = "1.1"
projects[revisionreference][patch][] = "http://drupal.org/files/entity-integration.patch"

