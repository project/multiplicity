api = 2
core = 7.x

includes[] = drupal-org.make

projects[phpmailer][subdir] = contrib
projects[phpmailer][version] = "3.x-dev"

libraries[phpmailer][download][type] = "get"
libraries[phpmailer][download][url] = "http://phpmailer.apache-extras.org.codespot.com/files/PHPMailer_5.2.1.tgz"
libraries[phpmailer][directory_name] = "phpmailer"
libraries[phpmailer][destination] = "libraries"

