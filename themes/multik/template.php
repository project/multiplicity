<?php

function multik_process_page(&$vars) {
  if (isset($vars['node'])) {
    $node = $vars['node'];
    $arg = arg(2);
    //if (in_array($node->type, array('message', 'response')) && node_is_page($node)) {
    if (in_array($node->type, array('message', 'response')) && ($arg == 'view' || empty($arg))) {
      $vars['title'] = array();
    }
  }
}

/*
function multik_process_node(&$vars) {
  $node = $vars['node'];
  if ($node->type == 'message' && $vars['view_mode'] == 'full') {
    if (!empty($node->field_message_child['und'][0]['nid'])) {
      unset($vars['content']['field_message_child']);
      $quoted = node_load($node->field_message_child['und'][0]['nid']);
      $vars['quoted'] = node_view($quoted, 'quoted');
    }
  }
  elseif ($node->type == 'response' && $vars['view_mode'] == 'full') {
    if (!empty($node->field_response_parent['und'][0]['nid'])) {
      unset($vars['content']['field_response_parent']);
      $quoted = node_load($node->field_response_parent['und'][0]['nid']);
      $vars['quoted'] = node_view($quoted, 'quoted');
    }
  }
}
*/

