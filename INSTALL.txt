Installing Multiplicity
=======================

From a release
--------------

 * Download 'multiplicity-7.x-1.x-core' and extract into your web directory (like you would normal Drupal core)

 * Install Drupal selecting the "Multiplicity" profile

 * Add a Mailbox

     http://localhost/admin/structure/mailhandler/add

 * Run the importer

     http://localhost/import/email_importer

 * Configure PHPMailer to send from the same address:

     http://localhost/admin/config/system/phpmailer

 * And I also recomment making sure the site e-mail address is the same and
   changing your "Site name" to what you want the From: name to be:

     http://localhost/admin/config/system/site-information

For development
---------------

 * Download the latest version of Drupal 7.x

     http://drupal.org/project/drupal
 
 * Clone the git repo into drupal-7.x/profiles

     http://drupal.org/project/multiplicity/git-instructions

 * Download contrib modules using the Drush makefile

     cd drupal-7.x/profiles/multiplicity
     ./rebuild.sh

 * Continue installing Drupal just like from a release

