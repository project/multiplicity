<?php

function messageresponse_send_confirm($form, &$form_state, $node) {
  //$form['response'] = node_view($node, 'quoted');
  //unset($form['response']['#contextual_links']);
  //unset($form['response']['links']);

  $form['node'] = array('#type' => 'value', '#value' => $node);

  return confirm_form($form,
    t('Are you sure that you want to send %title?', array('%title' => $node->title)),
    'node/' . $node->nid,
    t('This action cannot be undone.'),
    t('Send'),
    t('Cancel')
  );
}

function messageresponse_send_confirm_submit($form, &$form_state) {
  $node = $form_state['values']['node'];
  $parent = node_load($node->field_response_parent['und'][0]['nid']);

  if ($form_state['values']['confirm']) {
    $module = 'messageresponse';
    $token  = 'response';
    $from = variable_get('site_mail', $parent->field_message_to['und'][0]['value']);
    $to = $parent->field_message_from['und'][0]['value'];

    $mail = array(
      'id' => $module . '_' . $token,
      'from' => $from,
      'to' => $to,
      'subject' => $node->title,
      'body' => $node->body['und'][0]['value'],
      'headers' => array(
        'From' => $from,
        'Sender' => $from,
        'Return-Path' => $from,
      ),
    );

    $mail_system = drupal_mail_system($module, $token);
    if ($mail_system && $mail_system->mail($mail)) {
      $node->field_response_status['und'][0]['value'] = 'sent';
      $node->revision = 0;
      node_save($node);

      $parent->field_message_status['und'][0]['value'] = 'closed';
      node_save($parent);

      drupal_set_message('Sent!');

      $form_state['redirect'] = 'node/' . $node->nid;
    }
    else {
      drupal_set_message('Error sending message', 'error');
    }
  }
}

