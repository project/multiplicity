<?php
/**
 * @file
 * messageresponse.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function messageresponse_user_default_permissions() {
  $permissions = array();

  // Exported permission: create field_message_child.
  $permissions['create field_message_child'] = array(
    'name' => 'create field_message_child',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_message_date.
  $permissions['create field_message_date'] = array(
    'name' => 'create field_message_date',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_message_from.
  $permissions['create field_message_from'] = array(
    'name' => 'create field_message_from',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_message_from_raw.
  $permissions['create field_message_from_raw'] = array(
    'name' => 'create field_message_from_raw',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_message_status.
  $permissions['create field_message_status'] = array(
    'name' => 'create field_message_status',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_message_to.
  $permissions['create field_message_to'] = array(
    'name' => 'create field_message_to',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_response_parent.
  $permissions['create field_response_parent'] = array(
    'name' => 'create field_response_parent',
    'roles' => array(
      0 => 'administrator',
      1 => 'responder',
      2 => 'sender',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create field_response_status.
  $permissions['create field_response_status'] = array(
    'name' => 'create field_response_status',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: create response content.
  $permissions['create response content'] = array(
    'name' => 'create response content',
    'roles' => array(
      0 => 'administrator',
      1 => 'responder',
      2 => 'sender',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any message content.
  $permissions['edit any message content'] = array(
    'name' => 'edit any message content',
    'roles' => array(
      0 => 'administrator',
      1 => 'responder',
      2 => 'sender',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any response content.
  $permissions['edit any response content'] = array(
    'name' => 'edit any response content',
    'roles' => array(
      0 => 'administrator',
      1 => 'responder',
      2 => 'sender',
    ),
    'module' => 'node',
  );

  // Exported permission: edit field_message_child.
  $permissions['edit field_message_child'] = array(
    'name' => 'edit field_message_child',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_message_date.
  $permissions['edit field_message_date'] = array(
    'name' => 'edit field_message_date',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_message_from.
  $permissions['edit field_message_from'] = array(
    'name' => 'edit field_message_from',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_message_from_raw.
  $permissions['edit field_message_from_raw'] = array(
    'name' => 'edit field_message_from_raw',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_message_status.
  $permissions['edit field_message_status'] = array(
    'name' => 'edit field_message_status',
    'roles' => array(
      0 => 'administrator',
      1 => 'sender',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_message_to.
  $permissions['edit field_message_to'] = array(
    'name' => 'edit field_message_to',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_response_parent.
  $permissions['edit field_response_parent'] = array(
    'name' => 'edit field_response_parent',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit field_response_status.
  $permissions['edit field_response_status'] = array(
    'name' => 'edit field_response_status',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_message_child.
  $permissions['edit own field_message_child'] = array(
    'name' => 'edit own field_message_child',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_message_date.
  $permissions['edit own field_message_date'] = array(
    'name' => 'edit own field_message_date',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_message_from.
  $permissions['edit own field_message_from'] = array(
    'name' => 'edit own field_message_from',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_message_from_raw.
  $permissions['edit own field_message_from_raw'] = array(
    'name' => 'edit own field_message_from_raw',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_message_status.
  $permissions['edit own field_message_status'] = array(
    'name' => 'edit own field_message_status',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_message_to.
  $permissions['edit own field_message_to'] = array(
    'name' => 'edit own field_message_to',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_response_parent.
  $permissions['edit own field_response_parent'] = array(
    'name' => 'edit own field_response_parent',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own field_response_status.
  $permissions['edit own field_response_status'] = array(
    'name' => 'edit own field_response_status',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: edit own message content.
  $permissions['edit own message content'] = array(
    'name' => 'edit own message content',
    'roles' => array(
      0 => 'administrator',
      1 => 'responder',
      2 => 'sender',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own response content.
  $permissions['edit own response content'] = array(
    'name' => 'edit own response content',
    'roles' => array(
      0 => 'administrator',
      1 => 'responder',
      2 => 'sender',
    ),
    'module' => 'node',
  );

  // Exported permission: import email_importer feeds.
  $permissions['import email_importer feeds'] = array(
    'name' => 'import email_importer feeds',
    'roles' => array(
      0 => 'administrator',
      1 => 'responder',
      2 => 'sender',
    ),
    'module' => 'feeds',
  );

  // Exported permission: send response.
  $permissions['send response'] = array(
    'name' => 'send response',
    'roles' => array(
      0 => 'administrator',
      1 => 'sender',
    ),
    'module' => 'messageresponse',
  );

  // Exported permission: view field_message_child.
  $permissions['view field_message_child'] = array(
    'name' => 'view field_message_child',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_message_date.
  $permissions['view field_message_date'] = array(
    'name' => 'view field_message_date',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_message_from.
  $permissions['view field_message_from'] = array(
    'name' => 'view field_message_from',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_message_from_raw.
  $permissions['view field_message_from_raw'] = array(
    'name' => 'view field_message_from_raw',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_message_status.
  $permissions['view field_message_status'] = array(
    'name' => 'view field_message_status',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_message_to.
  $permissions['view field_message_to'] = array(
    'name' => 'view field_message_to',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_response_parent.
  $permissions['view field_response_parent'] = array(
    'name' => 'view field_response_parent',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view field_response_status.
  $permissions['view field_response_status'] = array(
    'name' => 'view field_response_status',
    'roles' => array(
      0 => 'administrator',
      1 => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_message_child.
  $permissions['view own field_message_child'] = array(
    'name' => 'view own field_message_child',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_message_date.
  $permissions['view own field_message_date'] = array(
    'name' => 'view own field_message_date',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_message_from.
  $permissions['view own field_message_from'] = array(
    'name' => 'view own field_message_from',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_message_from_raw.
  $permissions['view own field_message_from_raw'] = array(
    'name' => 'view own field_message_from_raw',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_message_status.
  $permissions['view own field_message_status'] = array(
    'name' => 'view own field_message_status',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_message_to.
  $permissions['view own field_message_to'] = array(
    'name' => 'view own field_message_to',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_response_parent.
  $permissions['view own field_response_parent'] = array(
    'name' => 'view own field_response_parent',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: view own field_response_status.
  $permissions['view own field_response_status'] = array(
    'name' => 'view own field_response_status',
    'roles' => array(
      0 => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
