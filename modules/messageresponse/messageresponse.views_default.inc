<?php
/**
 * @file
 * messageresponse.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function messageresponse_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'messages';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'messages';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Messages';
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'send response';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'field_message_from' => 'field_message_from',
    'title' => 'title',
    'field_message_date' => 'field_message_date',
    'field_message_assigned_to' => 'field_message_assigned_to',
    'field_message_status' => 'field_message_status',
  );
  $handler->display->display_options['style_options']['default'] = 'field_message_date';
  $handler->display->display_options['style_options']['info'] = array(
    'field_message_from' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'title' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_message_date' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_message_assigned_to' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_message_status' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  $handler->display->display_options['style_options']['override'] = 1;
  $handler->display->display_options['style_options']['sticky'] = 1;
  $handler->display->display_options['style_options']['empty_table'] = 0;
  /* Field: Content: From */
  $handler->display->display_options['fields']['field_message_from']['id'] = 'field_message_from';
  $handler->display->display_options['fields']['field_message_from']['table'] = 'field_data_field_message_from';
  $handler->display->display_options['fields']['field_message_from']['field'] = 'field_message_from';
  $handler->display->display_options['fields']['field_message_from']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_message_from']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_message_from']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_message_from']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_message_from']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_message_from']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_message_from']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_message_from']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_message_from']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_message_from']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_message_from']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_message_from']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_message_from']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_message_from']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['field_message_from']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_message_from']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_message_from']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_message_from']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_message_from']['field_api_classes'] = 0;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Subject';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['title']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Field: Content: Date */
  $handler->display->display_options['fields']['field_message_date']['id'] = 'field_message_date';
  $handler->display->display_options['fields']['field_message_date']['table'] = 'field_data_field_message_date';
  $handler->display->display_options['fields']['field_message_date']['field'] = 'field_message_date';
  $handler->display->display_options['fields']['field_message_date']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_message_date']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_message_date']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_message_date']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_message_date']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_message_date']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_message_date']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_message_date']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_message_date']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_message_date']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_message_date']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_message_date']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_message_date']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_message_date']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['field_message_date']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_message_date']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_message_date']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_message_date']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_message_date']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  $handler->display->display_options['fields']['field_message_date']['field_api_classes'] = 0;
  /* Field: Content: Assigned to */
  $handler->display->display_options['fields']['field_message_assigned_to']['id'] = 'field_message_assigned_to';
  $handler->display->display_options['fields']['field_message_assigned_to']['table'] = 'field_data_field_message_assigned_to';
  $handler->display->display_options['fields']['field_message_assigned_to']['field'] = 'field_message_assigned_to';
  $handler->display->display_options['fields']['field_message_assigned_to']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_message_assigned_to']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_message_assigned_to']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_message_assigned_to']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_message_assigned_to']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_message_assigned_to']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_message_assigned_to']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_message_assigned_to']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_message_assigned_to']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_message_assigned_to']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_message_assigned_to']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_message_assigned_to']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_message_assigned_to']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_message_assigned_to']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['field_message_assigned_to']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_message_assigned_to']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_message_assigned_to']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_message_assigned_to']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_message_assigned_to']['field_api_classes'] = 0;
  /* Field: Content: Status */
  $handler->display->display_options['fields']['field_message_status']['id'] = 'field_message_status';
  $handler->display->display_options['fields']['field_message_status']['table'] = 'field_data_field_message_status';
  $handler->display->display_options['fields']['field_message_status']['field'] = 'field_message_status';
  $handler->display->display_options['fields']['field_message_status']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_message_status']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_message_status']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_message_status']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_message_status']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_message_status']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_message_status']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_message_status']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_message_status']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_message_status']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_message_status']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_message_status']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_message_status']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_message_status']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['field_message_status']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_message_status']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_message_status']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_message_status']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_message_status']['field_api_classes'] = 0;
  /* Sort criterion: Content: Date (field_message_date) */
  $handler->display->display_options['sorts']['field_message_date_value']['id'] = 'field_message_date_value';
  $handler->display->display_options['sorts']['field_message_date_value']['table'] = 'field_data_field_message_date';
  $handler->display->display_options['sorts']['field_message_date_value']['field'] = 'field_message_date_value';
  $handler->display->display_options['filter_groups']['groups'] = array(
    1 => 'AND',
    2 => 'OR',
  );
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'message' => 'message',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;

  /* Display: My work */
  $handler = $view->new_display('page', 'My work', 'my_work');
  $handler->display->display_options['defaults']['footer'] = FALSE;
  $handler->display->display_options['defaults']['empty'] = FALSE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = FALSE;
  $handler->display->display_options['empty']['area']['content'] = '<p>No messages!<p>

<p>If you haven\'t added a mailbox, please do that <a href="/admin/structure/mailhandler/add">here</a> and then <a href="/import/email_importer">run the importer</a>.</p>';
  $handler->display->display_options['empty']['area']['format'] = 'full_html';
  $handler->display->display_options['empty']['area']['tokenize'] = 0;
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Content: Assigned to (field_message_assigned_to) */
  $handler->display->display_options['relationships']['field_message_assigned_to_uid']['id'] = 'field_message_assigned_to_uid';
  $handler->display->display_options['relationships']['field_message_assigned_to_uid']['table'] = 'field_data_field_message_assigned_to';
  $handler->display->display_options['relationships']['field_message_assigned_to_uid']['field'] = 'field_message_assigned_to_uid';
  $handler->display->display_options['relationships']['field_message_assigned_to_uid']['label'] = 'assigned_to';
  $handler->display->display_options['relationships']['field_message_assigned_to_uid']['required'] = 0;
  $handler->display->display_options['relationships']['field_message_assigned_to_uid']['delta'] = '0';
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'message' => 'message',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Status (field_message_status) */
  $handler->display->display_options['filters']['field_message_status_value']['id'] = 'field_message_status_value';
  $handler->display->display_options['filters']['field_message_status_value']['table'] = 'field_data_field_message_status';
  $handler->display->display_options['filters']['field_message_status_value']['field'] = 'field_message_status_value';
  $handler->display->display_options['filters']['field_message_status_value']['value'] = array(
    'open' => 'open',
  );
  $handler->display->display_options['filters']['field_message_status_value']['group'] = 1;
  /* Filter criterion: User: Current */
  $handler->display->display_options['filters']['uid_current']['id'] = 'uid_current';
  $handler->display->display_options['filters']['uid_current']['table'] = 'users';
  $handler->display->display_options['filters']['uid_current']['field'] = 'uid_current';
  $handler->display->display_options['filters']['uid_current']['relationship'] = 'field_message_assigned_to_uid';
  $handler->display->display_options['filters']['uid_current']['value'] = '1';
  $handler->display->display_options['filters']['uid_current']['group'] = 2;
  /* Filter criterion: Content: Assigned to (field_message_assigned_to) */
  $handler->display->display_options['filters']['field_message_assigned_to_uid']['id'] = 'field_message_assigned_to_uid';
  $handler->display->display_options['filters']['field_message_assigned_to_uid']['table'] = 'field_data_field_message_assigned_to';
  $handler->display->display_options['filters']['field_message_assigned_to_uid']['field'] = 'field_message_assigned_to_uid';
  $handler->display->display_options['filters']['field_message_assigned_to_uid']['operator'] = 'empty';
  $handler->display->display_options['filters']['field_message_assigned_to_uid']['group'] = 2;
  $handler->display->display_options['path'] = 'messages/my';
  $handler->display->display_options['menu']['type'] = 'default tab';
  $handler->display->display_options['menu']['title'] = 'My work';
  $handler->display->display_options['menu']['weight'] = '-10';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['tab_options']['type'] = 'normal';
  $handler->display->display_options['tab_options']['title'] = 'Messages';
  $handler->display->display_options['tab_options']['weight'] = '0';

  /* Display: Search */
  $handler = $view->new_display('page', 'Search', 'page_1');
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'message' => 'message',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Search: Search Terms */
  $handler->display->display_options['filters']['keys']['id'] = 'keys';
  $handler->display->display_options['filters']['keys']['table'] = 'search_index';
  $handler->display->display_options['filters']['keys']['field'] = 'keys';
  $handler->display->display_options['filters']['keys']['group'] = 1;
  $handler->display->display_options['filters']['keys']['exposed'] = TRUE;
  $handler->display->display_options['filters']['keys']['expose']['operator_id'] = 'keys_op';
  $handler->display->display_options['filters']['keys']['expose']['label'] = 'Search';
  $handler->display->display_options['filters']['keys']['expose']['operator'] = 'keys_op';
  $handler->display->display_options['filters']['keys']['expose']['identifier'] = 'search';
  $handler->display->display_options['filters']['keys']['expose']['multiple'] = FALSE;
  /* Filter criterion: Content: From (field_message_from) */
  $handler->display->display_options['filters']['field_message_from_value']['id'] = 'field_message_from_value';
  $handler->display->display_options['filters']['field_message_from_value']['table'] = 'field_data_field_message_from';
  $handler->display->display_options['filters']['field_message_from_value']['field'] = 'field_message_from_value';
  $handler->display->display_options['filters']['field_message_from_value']['group'] = 1;
  $handler->display->display_options['filters']['field_message_from_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_message_from_value']['expose']['operator_id'] = 'field_message_from_value_op';
  $handler->display->display_options['filters']['field_message_from_value']['expose']['label'] = 'From';
  $handler->display->display_options['filters']['field_message_from_value']['expose']['operator'] = 'field_message_from_value_op';
  $handler->display->display_options['filters']['field_message_from_value']['expose']['identifier'] = 'from';
  $handler->display->display_options['filters']['field_message_from_value']['expose']['required'] = 0;
  $handler->display->display_options['filters']['field_message_from_value']['expose']['multiple'] = FALSE;
  /* Filter criterion: Content: Assigned to (field_message_assigned_to) */
  $handler->display->display_options['filters']['field_message_assigned_to_uid']['id'] = 'field_message_assigned_to_uid';
  $handler->display->display_options['filters']['field_message_assigned_to_uid']['table'] = 'field_data_field_message_assigned_to';
  $handler->display->display_options['filters']['field_message_assigned_to_uid']['field'] = 'field_message_assigned_to_uid';
  $handler->display->display_options['filters']['field_message_assigned_to_uid']['group'] = 1;
  $handler->display->display_options['filters']['field_message_assigned_to_uid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_message_assigned_to_uid']['expose']['operator_id'] = 'field_message_assigned_to_uid_op';
  $handler->display->display_options['filters']['field_message_assigned_to_uid']['expose']['label'] = 'Assigned to';
  $handler->display->display_options['filters']['field_message_assigned_to_uid']['expose']['operator'] = 'field_message_assigned_to_uid_op';
  $handler->display->display_options['filters']['field_message_assigned_to_uid']['expose']['identifier'] = 'assigned';
  $handler->display->display_options['filters']['field_message_assigned_to_uid']['expose']['reduce'] = 0;
  /* Filter criterion: Content: Status (field_message_status) */
  $handler->display->display_options['filters']['field_message_status_value']['id'] = 'field_message_status_value';
  $handler->display->display_options['filters']['field_message_status_value']['table'] = 'field_data_field_message_status';
  $handler->display->display_options['filters']['field_message_status_value']['field'] = 'field_message_status_value';
  $handler->display->display_options['filters']['field_message_status_value']['value'] = array(
    'open' => 'open',
  );
  $handler->display->display_options['filters']['field_message_status_value']['group'] = 1;
  $handler->display->display_options['filters']['field_message_status_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_message_status_value']['expose']['operator_id'] = 'field_message_status_value_op';
  $handler->display->display_options['filters']['field_message_status_value']['expose']['label'] = 'Status';
  $handler->display->display_options['filters']['field_message_status_value']['expose']['operator'] = 'field_message_status_value_op';
  $handler->display->display_options['filters']['field_message_status_value']['expose']['identifier'] = 'status';
  $handler->display->display_options['filters']['field_message_status_value']['expose']['reduce'] = 0;
  $handler->display->display_options['path'] = 'messages/search';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Search';
  $handler->display->display_options['menu']['weight'] = '10';
  $handler->display->display_options['menu']['context'] = 0;

  /* Display: Messages from user */
  $handler = $view->new_display('entity_view', 'Messages from user', 'messages_from_user');
  $handler->display->display_options['defaults']['link_display'] = FALSE;
  $handler->display->display_options['link_display'] = 'my_work';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: From (only e-mail) (field_message_from_raw) */
  $handler->display->display_options['arguments']['field_message_from_raw_value']['id'] = 'field_message_from_raw_value';
  $handler->display->display_options['arguments']['field_message_from_raw_value']['table'] = 'field_data_field_message_from_raw';
  $handler->display->display_options['arguments']['field_message_from_raw_value']['field'] = 'field_message_from_raw_value';
  $handler->display->display_options['arguments']['field_message_from_raw_value']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['field_message_from_raw_value']['title_enable'] = 1;
  $handler->display->display_options['arguments']['field_message_from_raw_value']['title'] = 'Messages from %1';
  $handler->display->display_options['arguments']['field_message_from_raw_value']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_message_from_raw_value']['default_argument_skip_url'] = 0;
  $handler->display->display_options['arguments']['field_message_from_raw_value']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_message_from_raw_value']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_message_from_raw_value']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_message_from_raw_value']['glossary'] = 0;
  $handler->display->display_options['arguments']['field_message_from_raw_value']['limit'] = '0';
  $handler->display->display_options['arguments']['field_message_from_raw_value']['transform_dash'] = 0;
  $handler->display->display_options['arguments']['field_message_from_raw_value']['break_phrase'] = 0;
  $handler->display->display_options['entity_type'] = 'node';
  $handler->display->display_options['bundles'] = array(
    0 => 'message',
  );
  $handler->display->display_options['argument_mode'] = 'token';
  $handler->display->display_options['default_argument'] = '[node:field_message_from_raw]';
  $handler->display->display_options['show_title'] = 1;

  /* Display: Needs review */
  $handler = $view->new_display('page', 'Needs review', 'page_2');
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Content: Response (field_message_child) */
  $handler->display->display_options['relationships']['field_message_child_nid']['id'] = 'field_message_child_nid';
  $handler->display->display_options['relationships']['field_message_child_nid']['table'] = 'field_data_field_message_child';
  $handler->display->display_options['relationships']['field_message_child_nid']['field'] = 'field_message_child_nid';
  $handler->display->display_options['relationships']['field_message_child_nid']['label'] = 'response';
  $handler->display->display_options['relationships']['field_message_child_nid']['required'] = 1;
  $handler->display->display_options['relationships']['field_message_child_nid']['delta'] = '0';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: From */
  $handler->display->display_options['fields']['field_message_from']['id'] = 'field_message_from';
  $handler->display->display_options['fields']['field_message_from']['table'] = 'field_data_field_message_from';
  $handler->display->display_options['fields']['field_message_from']['field'] = 'field_message_from';
  $handler->display->display_options['fields']['field_message_from']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_message_from']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_message_from']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_message_from']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_message_from']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_message_from']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_message_from']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_message_from']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_message_from']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_message_from']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_message_from']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_message_from']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_message_from']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_message_from']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['field_message_from']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_message_from']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_message_from']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_message_from']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_message_from']['field_api_classes'] = 0;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['relationship'] = 'field_message_child_nid';
  $handler->display->display_options['fields']['title']['label'] = 'Subject';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['title']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Field: Content: Date */
  $handler->display->display_options['fields']['field_message_date']['id'] = 'field_message_date';
  $handler->display->display_options['fields']['field_message_date']['table'] = 'field_data_field_message_date';
  $handler->display->display_options['fields']['field_message_date']['field'] = 'field_message_date';
  $handler->display->display_options['fields']['field_message_date']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_message_date']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_message_date']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_message_date']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_message_date']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_message_date']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_message_date']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_message_date']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_message_date']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_message_date']['alter']['more_link'] = 0;
  $handler->display->display_options['fields']['field_message_date']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_message_date']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_message_date']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_message_date']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['field_message_date']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_message_date']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_message_date']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_message_date']['hide_alter_empty'] = 1;
  $handler->display->display_options['fields']['field_message_date']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
  );
  $handler->display->display_options['fields']['field_message_date']['field_api_classes'] = 0;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'message' => 'message',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: Status (field_response_status) */
  $handler->display->display_options['filters']['field_response_status_value']['id'] = 'field_response_status_value';
  $handler->display->display_options['filters']['field_response_status_value']['table'] = 'field_data_field_response_status';
  $handler->display->display_options['filters']['field_response_status_value']['field'] = 'field_response_status_value';
  $handler->display->display_options['filters']['field_response_status_value']['relationship'] = 'field_message_child_nid';
  $handler->display->display_options['filters']['field_response_status_value']['value'] = array(
    'draft' => 'draft',
  );
  $handler->display->display_options['path'] = 'messages/review';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Needs review';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $export['messages'] = $view;

  return $export;
}
