<?php
/**
 * @file
 * messageresponse.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function messageresponse_user_default_roles() {
  $roles = array();

  // Exported role: responder.
  $roles['responder'] = array(
    'name' => 'responder',
    'weight' => '4',
  );

  // Exported role: sender.
  $roles['sender'] = array(
    'name' => 'sender',
    'weight' => '3',
  );

  return $roles;
}
