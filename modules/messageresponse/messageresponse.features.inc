<?php
/**
 * @file
 * messageresponse.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function messageresponse_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "corresponding_node_references" && $api == "default_corresponding_node_references_presets") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function messageresponse_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function messageresponse_node_info() {
  $items = array(
    'message' => array(
      'name' => t('Message'),
      'base' => 'node_content',
      'description' => t('Incoming message.'),
      'has_title' => '1',
      'title_label' => t('Subject'),
      'help' => '',
    ),
    'response' => array(
      'name' => t('Response'),
      'base' => 'node_content',
      'description' => t('A proposed response.'),
      'has_title' => '1',
      'title_label' => t('Subject'),
      'help' => '',
    ),
  );
  return $items;
}
