<?php
/**
 * @file
 * messageresponse.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function messageresponse_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'email_importer';
  $feeds_importer->config = array(
    'name' => 'E-mail importer',
    'description' => 'Imports e-mail',
    'fetcher' => array(
      'plugin_key' => 'MailhandlerFetcher',
      'config' => array(
        'filter' => 'MailhandlerFilters',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'MailhandlerParser',
      'config' => array(
        'default_commands' => 'status: 1',
        'commands_failed_auth' => 'status: 0',
        'available_commands' => 'status',
        'command_plugin' => array(
          'MailhandlerCommandsHeaders' => 'MailhandlerCommandsHeaders',
          'MailhandlerCommandsDefault' => 'MailhandlerCommandsDefault',
          'MailhandlerCommandsFiles' => 'MailhandlerCommandsFiles',
        ),
        'authenticate_plugin' => 'MailhandlerAuthenticateDefault',
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'content_type' => 'message',
        'expire' => '-1',
        'author' => '0',
        'mappings' => array(
          0 => array(
            'source' => 'subject',
            'target' => 'title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'body_text',
            'target' => 'body',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'fromaddress',
            'target' => 'field_message_from',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'toaddress',
            'target' => 'field_message_to',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'date',
            'target' => 'field_message_date:start',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'fromaddress',
            'target' => 'field_message_from_raw',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'subject',
            'target' => 'title_field',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'input_format' => 'plain_text',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '0',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 1,
  );
  $export['email_importer'] = $feeds_importer;

  return $export;
}
