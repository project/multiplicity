<?php
/**
 * @file
 * multiplicity_activity.features.inc
 */

/**
 * Implements hook_views_api().
 */
function multiplicity_activity_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_default_message_type().
 */
function multiplicity_activity_default_message_type() {
  $items = array();
  $items['multiplicity_activity_create_comment'] = entity_import('message_type', '{
    "name" : "multiplicity_activity_create_comment",
    "description" : "Multiplicity - Create comment",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : { "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" } },
    "language" : "",
    "arguments" : null,
    "message_text" : { "und" : [
        {
          "value" : "\\u003Cp\\u003E\\u003Ca href=\\u0022[message:user:url]\\u0022\\u003E@{message:user:name}\\u003C\\/a\\u003E \\u003Ca href=\\u0022[message:field-activity-comment-ref:url]\\u0022\\u003Ecommented\\u003C\\/a\\u003E on \\u003Ca href=\\u0022[message:field-activity-node-ref:url]\\u0022\\u003E[message:field-activity-node-ref:title]\\u003C\\/a\\u003E\\u003C\\/p\\u003E\\r\\n\\u003Cdiv class=\\u0022multiplicity-activity-comment\\u0022\\u003E[message:field-activity-comment-ref:body]\\u003C\\/div\\u003E",
          "format" : "full_html",
          "safe_value" : "\\u003Cp\\u003E\\u003Ca href=\\u0022[message:user:url]\\u0022\\u003E@{message:user:name}\\u003C\\/a\\u003E \\u003Ca href=\\u0022[message:field-activity-comment-ref:url]\\u0022\\u003Ecommented\\u003C\\/a\\u003E on \\u003Ca href=\\u0022[message:field-activity-node-ref:url]\\u0022\\u003E[message:field-activity-node-ref:title]\\u003C\\/a\\u003E\\u003C\\/p\\u003E\\n\\u003Cdiv class=\\u0022multiplicity-activity-comment\\u0022\\u003E[message:field-activity-comment-ref:body]\\u003C\\/div\\u003E\\n"
        }
      ]
    }
  }');
  $items['multiplicity_activity_create_node'] = entity_import('message_type', '{
    "name" : "multiplicity_activity_create_node",
    "description" : "Multiplicity - Create response",
    "argument_keys" : [ "!user-picture" ],
    "argument" : [],
    "category" : "message_type",
    "data" : { "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" } },
    "language" : "en",
    "arguments" : [],
    "message_text" : { "und" : [
        {
          "value" : "\\u003Ca href=\\u0022[message:user:url]\\u0022\\u003E@{message:user:name}\\u003C\\/a\\u003E created \\u003Ca href=\\u0022[message:field-activity-node-ref:url]\\u0022\\u003E[message:field-activity-node-ref:title]\\u003C\\/a\\u003E\\r\\n",
          "format" : "full_html",
          "safe_value" : "\\u003Cp\\u003E\\u003Ca href=\\u0022[message:user:url]\\u0022\\u003E@{message:user:name}\\u003C\\/a\\u003E created \\u003Ca href=\\u0022[message:field-activity-node-ref:url]\\u0022\\u003E[message:field-activity-node-ref:title]\\u003C\\/a\\u003E\\u003C\\/p\\u003E\\n"
        }
      ]
    }
  }');
  $items['multiplicity_activity_update_response'] = entity_import('message_type', '{
    "name" : "multiplicity_activity_update_response",
    "description" : "Multiplicity - Update response",
    "argument_keys" : [],
    "argument" : [],
    "category" : "message_type",
    "data" : { "purge" : { "override" : 0, "enabled" : 0, "quota" : "", "days" : "" } },
    "language" : "",
    "arguments" : null,
    "message_text" : { "und" : [
        {
          "value" : "\\u003Cp\\u003E\\u003Ca href=\\u0022[message:user:url]\\u0022\\u003E@{message:user:name}\\u003C\\/a\\u003E modified the response \\u003Ca href=\\u0022[message:field-activity-node-ref:url]\\u0022\\u003E[message:field-activity-node-ref:title]\\u003C\\/a\\u003E\\u003C\\/p\\u003E\\r\\n[message:revision-info]",
          "format" : "full_html",
          "safe_value" : "\\u003Cp\\u003E\\u003Ca href=\\u0022[message:user:url]\\u0022\\u003E@{message:user:name}\\u003C\\/a\\u003E modified the response \\u003Ca href=\\u0022[message:field-activity-node-ref:url]\\u0022\\u003E[message:field-activity-node-ref:title]\\u003C\\/a\\u003E\\u003C\\/p\\u003E\\n\\u003Cp\\u003E[message:revision-info]\\u003C\\/p\\u003E\\n"
        }
      ]
    }
  }');
  return $items;
}
